import type { ComponentProps } from 'solid-js';
import camelcase from 'camelcase';
import { Icon as _Icon, type IconProps as _Props } from './component/icon.tsx';
import * as icons from './icons.tsx';
import type { Kebabcase } from './helpers.ts';

export interface IconProps extends Omit<_Props, 'component'> {
	name: Kebabcase<keyof typeof icons>;
}

export function Icon(props: IconProps) {
	const name = camelcase(props.name) as keyof typeof icons;

	if (!(name in icons)) {
		throw new Error(`No icon named "${props.name}"`);
	}

	const Paths = icons[name];
	const Node = (props: ComponentProps<'svg'>) => <svg
		xmlns="http://www.w3.org/2000/svg"
		width="24"
		height="24"
		viewBox="0 0 24 24"
		stroke-width="1.5"
		stroke="currentColor"
		fill="none"
		stroke-linecap="round"
		stroke-linejoin="round"
		{...props}
	><Paths /></svg>

	return <_Icon {...props} component={Node} />
}
