import { createMemo, splitProps } from 'solid-js';
import type { ComponentProps, ValidComponent, VoidProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';

import './icon.css';

export interface IconProps extends VoidProps<ComponentProps<'svg'>> {
	/**
	 * An accessible label for the icon.
	 * This label will be visually hidden but announced by
	 * screen-readers, similar to `alt` text for `img` tags.
	 * @default undefined
	 */
	label?: string;
	component: ValidComponent;
}

export function Icon (props: IconProps) {
	const [local, domProps] = splitProps(props, ['label']);

	const a11yProps = createMemo(() => local.label ? {
		role: 'img' as const,
		'aria-label': local.label,
	} : {
		'aria-hidden': true,
		focusable: 'false',
	});

	return <Dynamic {...domProps} class='icon' {...a11yProps} />
}
