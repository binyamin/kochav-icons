/** From `astro/dist/type-utils` */
type _Kebab<T extends string, A extends string = ''> = T extends `${infer F}${infer R}` ? _Kebab<R, `${A}${F extends Lowercase<F> ? '' : '-'}${Lowercase<F>}`> : A;

export type Kebabcase<T extends string> = _Kebab<T>;
export type Camelcase<T extends string> =
	T extends `${infer F}-${infer R}`
	? Camelcase<`${F}${Capitalize<R>}`>
	: T;
